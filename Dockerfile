FROM python:3.7-slim

COPY requirements.txt /usr/bin
WORKDIR /usr/bin
RUN pip install -r requirements.txt
COPY pipe pipe.yml /usr/bin/

ENTRYPOINT ["python3", "/usr/bin/main.py"]
